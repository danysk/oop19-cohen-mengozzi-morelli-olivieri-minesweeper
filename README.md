## Titolo Progetto

Minesweeper

## Sviluppato con

* JavaFX - GUI
* Junit - Test

## Autori

- Cohen David
- Mengozzi Maria
- Morelli Lorenzo
- Olivieri Luigi

## Istruzioni

Scaricare ed eseguire il file Minesweeper.jar. Assicurarsi che sia installata sul dispositivo una JRE funzionante.

# Guida Utente

Per iniziare una nuova partita, selezionare “Play Game”, si aprirà una nuova pagina in cui selezionare la modalità di gioco (Standard, 1 vs 1, Beat the Timer) e la difficoltà (Easy, Medium, Hard, Personalized).

Una volta scelte le opzioni di gioco cliccando sul pulsante “Play”, situato in basso a destra, sarà richiesto all’utente di inserire il suo nome per iniziare a giocare, in caso si ometta il nome il gioco partirà ugualmente ma il suo punteggio non verrà contato. 

Fatto ciò inizierà la partita: in una nuova pagina verrà generata una griglia di bottoni sotto ai quali si nascondono le mine (in numero proporzionato alla difficoltà scelta).

Per scoprire il contenuto di una casella basterà cliccarla con il tasto sinistro del mouse, mentre per inserire o rimuovere una bandiera bisognerà usare il tasto destro.

Se la casella cliccata non contiene una mina mostrerà il numero di mine ad essa adiacenti, se non ci sono mine in vicinanza non mostrerà nulla.

Lo scopo del gioco è aprire tutte le caselle non contenenti le mine.